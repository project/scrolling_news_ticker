# Scrolling News Ticker

This module is used to show scroll news ticker global site or specific node.
This module will be used for statistical and development purposes only.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/scrolling_news_ticker).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/scrolling_news_ticker).


## Table of contents

- Uses
- Requirements
- Installation
- Configuration
- Troubleshooting
- Maintainers


## Uses

- This module edit content in block section.

- This module manage font size in block form section.

- This module manage color in block form section.

- This module manage various direction in block form section.

- This module manage scroll speed in block form section.

- This module manage visibility pages, roles in block form section.

Note: This module is for statistical and development purpose only.
Enable it when ever its needed in the Production Environment.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Customize the menu settings in Administration ? Configuration and modules ?
   Administration ? Administration menu.

2. To prevent administrative menu items from appearing twice, you may hide the
   "Management" menu block.


## Troubleshooting

If not displayed updated content, check the following:

- Check you have administrator permission to view this module.
- Clear the drupal cache if is not displayed updated content.


## Maintainers

- Raubi Gaur - [raubi gaur](https://www.drupal.org/u/raubi-gaur)
- Deepak Bhati - [heni_deepak](https://www.drupal.org/u/heni_deepak)
- Kamlesh Kishor Jha - [Kamlesh Kishor Jha](https://www.drupal.org/u/kamlesh-kishor-jha)
- Gajendra Sharma - [gajendra_sharma](https://www.drupal.org/u/gajendra_sharma)
