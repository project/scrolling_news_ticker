<?php

namespace Drupal\scrolling_news_ticker\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Scrolling News Ticker' Block.
 *
 * @Block(
 *   id = "scrolling_news_ticker_block",
 *   admin_label = @Translation("Scrolling News Ticker"),
 *   category = @Translation("Scrolling News Ticker"),
 * )
 */
class ScrollingNewsTicker extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build = [
      '#type' => 'container',
      'label' => ['#markup' => ''],
    ];

    if (!empty($this->configuration['scrolling_news_ticker_block'])) {

      if ($this->configuration['scrolling_font_color']) {
        $fontColor = $this->configuration['scrolling_font_color'];
      }
      else {
        $fontColor = "#000";
      }
      if ($this->configuration['scrolling_direction']) {
        $direction = $this->configuration['scrolling_direction'];
      }
      else {
        $direction = "left";
      }
      if ($this->configuration['scrolling_number']) {
        $scrollamount = $this->configuration['scrolling_number'];
      }
      else {
        $direction = "2";
      }

      $build['scrolling_news_ticker_block'] = [
        '#type' => 'processed_text',
        '#text' => '<marquee scrollamount="' . $scrollamount . '" style="color:' . $fontColor . '" direction="' . $direction . '">' . $this->configuration['scrolling_news_ticker_block']['value'] . '</marquee>',
        '#format' => $this->configuration['scrolling_news_ticker_block']['format'],

      ];
    }

    return $build;

  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $body = !empty($this->configuration['scrolling_news_ticker_block']) ? $this->configuration['scrolling_news_ticker_block'] : [];

    $form['scrolling_news_ticker_block'] = [
      '#type' => 'text_format',
      '#title' => 'News Ticker Section',
      '#format' => $body['format'] ?? 'full_html',
      '#default_value' => $body['value'] ?? '',
    ];

    $form['scrolling_font_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Font Color'),
      '#description' => $this->t('To change font color'),
      '#default_value' => $this->configuration['scrolling_font_color'],
    ];

    $form['scrolling_direction'] = [
      '#type' => 'select',
      '#title' => t('Various direction'),
      '#options' => [
        'left' => t('Left Direction'),
        'right' => t('Right Direction'),
        'up' => t('Up Direction'),
        'down' => t('Down Direction'),
      ],
      '#default_value' => $this->configuration['scrolling_direction'],
    ];

    $form['scrolling_number'] = [
      '#type' => 'number',
      '#title' => $this->t('Scroll Speed'),
      '#description' => $this->t('To change scroll speed'),
      '#default_value' => $this->configuration['scrolling_number'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    $this->configuration['scrolling_news_ticker_block'] = $form_state->getValue('scrolling_news_ticker_block');
    $this->configuration['scrolling_font_color'] = $form_state->getValue('scrolling_font_color');
    $this->configuration['scrolling_direction'] = $form_state->getValue('scrolling_direction');
    $this->configuration['scrolling_number'] = $form_state->getValue('scrolling_number');
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    if ($form_state->getValue('scrolling_news_ticker_block') === '') {
      $form_state->setErrorByName('scrolling_news_ticker_block', $this->t('You can not empty news ticker section.'));
    }
  }

}
